<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Alumno</title>
</head>
<body>
<h1>
	Alumno concreto
</h1>
<ul>

<li><c:out value="${alumno.getNombre()}"></c:out></li>
<li><c:out value="${alumno.getApellidos()}"></c:out></li>
<li><c:out value="${alumno.getCentro()}"></c:out></li>
</ul>
</body>
</html>
