<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Alumnos
</h1>
<ul>
<c:forEach items="${alumnos}" var="alumno">
<li><a href="/springmybatis/alumno/${alumno.getIdalumno() }"><c:out value="${alumno.getNombre()}"></c:out></a></li>
</c:forEach>
</ul>
<P>  The time on the server is ${serverTime}. </P>
</body>
</html>
