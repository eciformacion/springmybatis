<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Alumno</title>
</head>
<body>
<h1>
	Notas alumno concreto
</h1>
<ul>

<li><c:out value="${notasalumno.getNombre()}"></c:out></li>
<li><c:out value="${notasalumno.getApellidos()}"></c:out></li>
<li><c:out value="${notasalumno.getCentro()}"></c:out></li>
</ul>
<table>
<thead>
<tr>
<th>Asignatura</th>
<th>Calificacion</th>
</tr>
</thead>
<tbody>
<c:forEach items="${notasalumno.getNotas() }" var="nota">
<tr>
<td><c:out value="${nota.getAsignatura() }"></c:out>
</td>
<td><c:out value="${nota.getCalificacion() }"></c:out>
</td>
</tr>
</c:forEach>
</tbody>
</table>
</body>
</html>
