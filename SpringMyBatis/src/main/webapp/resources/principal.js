/**
 * 
 */

$(function(){
	
	//Asociamos el evento click del boton de refrescar
	$("#actualizar").click(function(){
		cargarAlumnos();
	})
	
	//Asociamos el evento de click de cada alumno
	$("#listaAlumnos").on("click","li",function(){
		console.log($(this).attr("data-idalumno"));
		cargarNotas($(this).data("idalumno"));
	})
	
});

function cargarNotas(idalumno){
	
	$.ajax("http://localhost:8080/springmybatis/alumno/"+idalumno+"/notas",{
		success:function(data){
			$("#listaNotas").html("");
			var listanotas=data;
			for(var a in listanotas){
				$("#listaNotas")
				.append('<li  class="list-group-item">'+
						listanotas[a].asignatura+": "+
						listanotas[a].calificacion+
						"</li>");
			}
		}
	})

}
function cargarAlumnos(){
	$.ajax("http://localhost:8080/springmybatis/alumno",{
		success:function(data){
			$("#listaAlumnos").html("");
			var listaalumnos=data;
			for(var a in listaalumnos){
				$("#listaAlumnos")
				.append('<li data-idalumno="'+listaalumnos[a].idalumno+'" class="list-group-item alumno">'+
						listaalumnos[a].nombre+" "+
						listaalumnos[a].apellidos+
						"</li>");
			}
		}
	})


}