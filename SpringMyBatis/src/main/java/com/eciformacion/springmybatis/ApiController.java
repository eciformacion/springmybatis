package com.eciformacion.springmybatis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.springmybatis.persistencia.GestorDatos;
import com.eciformacion.springmybatis.persistencia.pojos.Alumno;
import com.eciformacion.springmybatis.persistencia.pojos.NotasAlumno;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/alumno")
public class ApiController {

	@Autowired
	private GestorDatos gestorDatos;
	
	public GestorDatos getGestorDatos() {
		return gestorDatos;
	}

	public void setGestorDatos(GestorDatos gestorDatos) {
		this.gestorDatos = gestorDatos;
	}
	@GetMapping
	public List getAllAlumno(){
		return (List)gestorDatos.getAlumnoMapper().getAlumnos();
	}
	@GetMapping("/{idval}")
	public Alumno  getAlumno(@PathVariable("idval") long idval){
		return gestorDatos.getAlumnoMapper().getAlumno(idval);
	}
	@GetMapping("/{idval}/notas")
	public List  getNotasAlumno(@PathVariable("idval") long idval){
		return gestorDatos.getAlumnoMapper().getNotasAlumno(idval).getNotas();
	}
	
	@PostMapping()
		public Alumno  postAlumno(@RequestBody Alumno alumno){
			 gestorDatos.getAlumnoMapper().nuevoAlumno(alumno);
	return alumno;	
	}
	
}
