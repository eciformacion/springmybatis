package com.eciformacion.springmybatis.persistencia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eciformacion.springmybatis.persistencia.mappers.AlumnoMapper;

@Component("dataSource")
public class GestorDatos {

	@Autowired
	private AlumnoMapper alumnoMapper;

	
	
	public AlumnoMapper getAlumnoMapper() {
		return alumnoMapper;
	}

	public void setAlumnoMapper(AlumnoMapper alumnoMapper) {
		this.alumnoMapper = alumnoMapper;
	}
	
}
