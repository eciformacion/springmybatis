package com.eciformacion.springmybatis.persistencia.mappers;

import java.util.List;

import com.eciformacion.springmybatis.persistencia.pojos.Alumno;
import com.eciformacion.springmybatis.persistencia.pojos.NotasAlumno;

public interface AlumnoMapper {

	public List<Alumno> getAlumnos();
	
	public Alumno getAlumno(long id);
	
	public int nuevoAlumno(Alumno al);
	public NotasAlumno getNotasAlumno(long id);
}
