package com.eciformacion.springmybatis.persistencia;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class CustomDataSource extends DriverManagerDataSource {
	public CustomDataSource() {
		super();
		this.setDriverClassName("com.mysql.jdbc.Driver");
		this.setUrl("jdbc:mysql://localhost:3307/prueba1");
		this.setUsername("root");
		this.setPassword("usbw");
	}
	
}
