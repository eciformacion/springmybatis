package com.eciformacion.springmybatis.persistencia.pojos;

import java.io.Serializable;

public class Nota implements Serializable {
	private int idnota;
	private int idalumno;
	private String asignatura;
	private int calificacion;
	public int getIdnota() {
		return idnota;
	}
	public void setIdnota(int idnota) {
		this.idnota = idnota;
	}
	public int getIdalumno() {
		return idalumno;
	}
	public void setIdalumno(int idalumno) {
		this.idalumno = idalumno;
	}
	public String getAsignatura() {
		return asignatura;
	}
	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}
	public int getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}
}
